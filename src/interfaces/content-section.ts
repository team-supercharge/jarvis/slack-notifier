import { ContentLine } from './content-line';

export interface ContentSection {
  header: string;
  lines: ContentLine[];
}
