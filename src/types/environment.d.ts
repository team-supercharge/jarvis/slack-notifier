declare global {
  namespace NodeJS {
    interface ProcessEnv {
      // mandatory config
      JARVIS_SLACK_BOT_TOKEN: string;
      JARVIS_SLACK_SIGNING_SECRET: string;
      JARVIS_SLACK_CHANNEL_ID: string;

      // optional config
      JARVIS_SLACK_GROUP_EMOJI: string;
      JARVIS_SLACK_GITLAB_PRIVATE_TOKEN: string;
      JARVIS_SLACK_NO_RELEASE_CONTENT: string;

      // gitlab pre-defined variables
      CI_SERVER_URL: string;
      CI_PROJECT_ID: string;
      CI_COMMIT_TAG: string;
      CI_PROJECT_NAMESPACE: string;
      CI_PROJECT_NAME: string;
      CI_PROJECT_TITLE: string;
      CI_PIPELINE_ID: string;
      CI_JOB_TOKEN: string;
      CI_JOB_STATUS: string;
    }
  }
}

export {};
