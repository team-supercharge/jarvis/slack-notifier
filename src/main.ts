// tslint:disable-next-line:no-var-requires
const dotenvSafe = require('dotenv-safe');

import fs from 'fs';

import { App } from './app';

async function main(argv: string[]): Promise<void> {
  const command = argv[2];

  if (!command) {
    throw Error('you must specify a command');
  }

  if (command === 'version') {
    const pjson = JSON.parse(fs.readFileSync(`${__dirname}/../package.json`, 'utf-8'));
    console.log(pjson.version);
    process.exit(0);
  }

  dotenvSafe.config({
    example: `${__dirname}/../.env.example`,
  });

  const app = new App();

  switch (command) {
    case 'build-start':
      await app.buildStart();
      break;

    case 'build-result':
      await app.buildResult();
      break;

    case 'build-success':
      await app.buildSuccess();
      break;

    case 'build-failed':
      await app.buildFailed();
      break;

    case 'build-canceled':
      await app.buildCanceled();
      break;

    case 'send-message':
      const message: string = argv[3];
      const blocksPath: string | undefined = argv[4];
      if (!message) {
        throw Error('you must specify a message');
      }

      await app.sendMessage(message, blocksPath);
      break;

    default:
      throw Error(`command '${command}' not found`);
  }
}

// run program
main(process.argv).catch(e => {
  console.error(e);
  process.exit(1);
});
