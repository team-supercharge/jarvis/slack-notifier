# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.2.5](https://gitlab.com/team-supercharge/jarvis/slack-notifier/compare/v1.2.4...v1.2.5) (2024-04-02)


### Build

* update to node 18.20.0 LTS ([68693e7](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/68693e7dabcdb5cb0f908c59c6c9f63ddd3edb15))

### [1.2.4](https://gitlab.com/team-supercharge/jarvis/slack-notifier/compare/v1.2.3...v1.2.4) (2023-05-04)


### CI

* check building docker image on merge requests ([7b77d41](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/7b77d41531f4419d30c6eb78891b62382cfe2458))


### Build

* update to node 18.16.0 LTS ([185d29a](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/185d29ad9a18b5d7cc32111f6a1128767ed08464))

### [1.2.3](https://gitlab.com/team-supercharge/jarvis/slack-notifier/compare/v1.2.2...v1.2.3) (2023-04-28)


### Bug Fixes

* handle 3000 character limit dynamically for change lines ([bb137ed](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/bb137edeeab9e38804151ce2b76f7cdd348c7d1f))
* sending custom messages not for a specific tag ([d1612ff](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/d1612ff67b96262583e47fa9b6db968e1d482d32))

### [1.2.2](https://gitlab.com/team-supercharge/jarvis/slack-notifier/compare/v1.2.1...v1.2.2) (2023-02-24)


### Bug Fixes

* missing breaking indicator from lines ([7962c73](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/7962c73f0b5a546c8ffccd78a0a924187841d8f8))

### [1.2.1](https://gitlab.com/team-supercharge/jarvis/slack-notifier/compare/v1.2.0...v1.2.1) (2023-02-23)


### Bug Fixes

* add missing deps to Dockerfile ([732e731](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/732e731b05a5ce71d9afdabb3a0caa450491616c))

## [1.2.0](https://gitlab.com/team-supercharge/jarvis/slack-notifier/compare/v1.1.0...v1.2.0) (2023-02-22)


### Features

* add divider to the beginning to improve readability for multiple releases ([84dc381](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/84dc38190eaf5466c62dfc0f17b602b4e46e53b6))
* add send-message command ([5cea2d8](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/5cea2d89779fcdeda26194d202e5c06e7afc0e98))
* change metadata section to a context tag to save vertical space ([a363d47](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/a363d471fa60c9a2f7d6667400215317f64c83b5))
* format change lines as quotes to improve readability ([f706528](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/f7065280fc8235903e46676b3fda72b6503f3e58))
* remove dividers to save some vertical space ([3ebfd8a](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/3ebfd8a2bc2f59ed40aae5ee3d19790d5bdbd42f))
* simplify breaking changes by merging it to other sections ([c766d86](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/c766d86562988e3ff2ef91dcde58a46ba45169ef))


### Documentation

* add usage section with reference to main JARVIS ([9f864df](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/9f864df5d19a27ac8f39192385778f24967126c4))


### CI

* update to jarvis@8.0.0 ([a858cfa](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/a858cfacfd2635e54e44e15b78ad1d5c85f1ddd3))

## [1.1.0](https://gitlab.com/team-supercharge/jarvis/slack-notifier/compare/v1.0.0...v1.1.0) (2022-10-17)


### Features

* add all supported header types with emojis ([fd55ad9](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/fd55ad9c4cd6ff886f42491453dab552d7affe70))

## [1.0.0](https://gitlab.com/team-supercharge/jarvis/slack-notifier/compare/v0.0.4...v1.0.0) (2022-10-17)


### Features

* add docker output artifact ([7db608c](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/7db608cba5561a987999a73dc945cf32902e1fc7))
* add flag to disable the content section ([7703da4](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/7703da4156f116b74d8bf7945b601fb32d7ea429))
* add version command for debugging purposes ([ac06545](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/ac065458b58b69a126d2b3a4898ad5d87a41e00d))

### [0.0.4](https://gitlab.com/team-supercharge/jarvis/slack-notifier/compare/v0.0.3...v0.0.4) (2022-10-14)


### Bug Fixes

* pipeline status link placed on icon insteaad of text ([c92d0fc](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/c92d0fc84162bf290112b1d99a78a3087b20455c))
* split long text chunks into multiple blocks to avoid Slack API limitations ([43b87b9](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/43b87b9097b962c218e24e360b826ec0220e9a48))
* wrongly composed project title url ([7cf7701](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/7cf7701ba63e46a58ebc385fd003423052e957a0))

### [0.0.3](https://gitlab.com/team-supercharge/jarvis/slack-notifier/compare/v0.0.2...v0.0.3) (2022-10-14)


### Features

* add basic Slack app manifest ([0240bfb](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/0240bfb636add1835ff027565fc2ece19f1dd5ac))
* configure sending notifications while building self ([71a620f](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/71a620f4b6c80e72eb28e1b41a8073b5a810b473))
* handle build result based on predefined gitlab variable ([477ed0e](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/477ed0eb289fed41900427fcc5b91f6ebb029d27))
* handle canceled status for releases ([4fb8264](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/4fb8264fd569e2c64cfc109f7f938ce8cb92c374))
* rename and ignore ts file ([8994cc9](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/8994cc94c07daace1260c22a8bed096e54ae6130))


### Bug Fixes

* rename commands and methods to be consistent with gitlab statuses ([98f2fc7](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/98f2fc76e036a9724681ef7caf98378d97a8c0c2))

### [0.0.2](https://gitlab.com/team-supercharge/jarvis/slack-notifier/compare/v0.0.1...v0.0.2) (2022-10-13)


### Features

* clean up environment variables & add them to README ([6b62139](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/6b62139c1f7d9022d77befced350bc5c9cb0544c))

### 0.0.1 (2022-10-13)


### Features

* configure CI build and npmjs.org publish ([2de1a67](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/2de1a6774bf4bc6ffc4445f0d4d8e822aab0724d))
* handle first ever release correctly ([9e47f3e](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/9e47f3e194a34ed8b1242644cd0ad76daa127b7f))
* initialize Slack app with Bolt.js and send release messages with mock data ([5e37abe](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/5e37abe3d5d1d9e8df3fb396edc1c03d9d22ed66))
* parse release data from GitLab API ([60a04f9](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/60a04f9cdd12fa5e3b4c596c8757b94cd718cfa0))
* prepare app to receive cli commands ([642451b](https://gitlab.com/team-supercharge/jarvis/slack-notifier/commit/642451b2fa541773641fa929bc0102a56b36b4cf))
