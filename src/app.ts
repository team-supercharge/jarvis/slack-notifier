import fs from 'fs';

import { GitlabService } from './services/gitlab-service';
import { SlackService } from './services/slack-service';
import { ReleaseStatus } from './enums/release-status';
import { BlockKitObject } from './interfaces/block-kit-object';

const TS_FILE_PATH = '.jarvis-slack-release-ts';

export class App {
  private readonly gitlab: GitlabService;
  private readonly slack: SlackService;

  constructor() {
    this.gitlab = new GitlabService();
    this.slack = new SlackService();
  }

  public async buildStart(): Promise<void> {
    const model = await this.gitlab.resolveRelease();
    const ts = await this.slack.sendReleaseNotification(model);

    this.writeTs(ts);
  }

  public async buildResult(): Promise<void> {
    switch (process.env.CI_JOB_STATUS) {
      case 'success':
        await this.buildSuccess();
        break;

      case 'failed':
        await this.buildFailed();
        break;

      case 'canceled':
        await this.buildCanceled();
        break;

      default:
        throw Error(`status '${process.env.CI_JOB_STATUS}' not found`);
    }
  }

  public async buildSuccess(): Promise<void> {
    const ts = this.readTs();

    const model = await this.gitlab.resolveRelease();
    await this.slack.updateReleaseNotification(ts, model, ReleaseStatus.READY);
  }

  public async buildFailed(): Promise<void> {
    const ts = this.readTs();

    const model = await this.gitlab.resolveRelease();
    await this.slack.updateReleaseNotification(ts, model, ReleaseStatus.FAILED);
  }

  public async buildCanceled(): Promise<void> {
    const ts = this.readTs();

    const model = await this.gitlab.resolveRelease();
    await this.slack.updateReleaseNotification(ts, model, ReleaseStatus.CANCELED);
  }

  public async sendMessage(message: string, blocksPath?: string): Promise<void> {
    let blockKitObject: BlockKitObject | undefined;
    try {
      if (blocksPath) {
        // check if file exists and is readable
        fs.accessSync(blocksPath, fs.constants.R_OK);
        // read file content
        const blocksFileContent = fs.readFileSync(blocksPath, 'utf-8');
        // check if it's a valid JSON
        blockKitObject = JSON.parse(blocksFileContent) as BlockKitObject;
      }
    } catch (error) {
      console.error(`Error reading ${blocksPath}: ${error}`);

      return;
    }
    await this.slack.sendMessage(message, blockKitObject);
  }

  private writeTs(ts: string): void {
    try {
      fs.writeFileSync(TS_FILE_PATH, ts);
    } catch (e) {
      throw Error(`Error writing file ${TS_FILE_PATH}`);
    }
  }

  private readTs(): string {
    let ts;
    try {
      ts = fs.readFileSync(TS_FILE_PATH, 'utf-8');
    } catch (e) {
      throw Error(`Error reading file ${TS_FILE_PATH}`);
    }

    return ts;
  }
}
