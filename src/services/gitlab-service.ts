import { Gitlab } from '@gitbeaker/node';

import { ReleaseMessageModel } from '../interfaces/release-message-model';
import { ContentSection } from '../interfaces/content-section';

const breakingChangesTitle = 'BREAKING CHANGES';

export class GitlabService {
  private readonly api;

  constructor() {
    if (process.env.JARVIS_SLACK_GITLAB_PRIVATE_TOKEN) {
      this.api = new Gitlab({
        host: process.env.CI_SERVER_URL,
        token: process.env.JARVIS_SLACK_GITLAB_PRIVATE_TOKEN,
      });
    } else {
      this.api = new Gitlab({
        host: process.env.CI_SERVER_URL,
        jobToken: process.env.CI_JOB_TOKEN,
      });
    }
  }

  public async resolveRelease(): Promise<ReleaseMessageModel> {
    const release = await this.api.Releases.show(process.env.CI_PROJECT_ID, process.env.CI_COMMIT_TAG);

    console.log(JSON.stringify(release));
    const { compareUrl, sections, breaking } = this.parseContent(release.description);

    // HACK: @gitbeaker/node has wrong type mapping
    const author: { name: string; avatar_url: string; web_url: string } = JSON.parse(JSON.stringify(release)).author;

    return {
      name: release.name,
      breaking,
      compareUrl,
      namespaceUrl: this.gitlabUrl(`/${process.env.CI_PROJECT_NAMESPACE}`),
      project: {
        title: process.env.CI_PROJECT_TITLE,
        url: this.gitlabProjectUrl(),
      },
      tag: {
        name: release.tag_name,
        url: this.gitlabUrl(release.tag_path),
      },
      commit: {
        name: release.commit.short_id,
        url: this.gitlabTagGraphUrl(release.tag_name),
      },
      sourceCodeUrl: release.assets.sources?.find(source => source.format === 'zip')?.url,
      pipeline: {
        user: {
          name: author?.name,
          profileUrl: author?.web_url,
          avatarUrl: author?.avatar_url,
        },
        url: this.gitlabPipelineUrl(process.env.CI_PIPELINE_ID),
      },
      contentSections: sections,
    };
  }

  private gitlabUrl(path?: string): string {
    return `${process.env.CI_SERVER_URL}${path || ''}`;
  }

  private gitlabProjectUrl(path?: string): string {
    return this.gitlabUrl(`/${process.env.CI_PROJECT_NAMESPACE}/${process.env.CI_PROJECT_NAME}${path || ''}`);
  }

  private gitlabTagGraphUrl(tagName: string): string {
    return this.gitlabProjectUrl(`/-/network/${tagName}`);
  }

  private gitlabPipelineUrl(pipelineId: string): string {
    return this.gitlabProjectUrl(`/-/pipelines/${pipelineId}`);
  }

  private parseContent(changes: string): { compareUrl?: string; sections: ContentSection[]; breaking: boolean } {
    // remove first line (title)
    const lines = changes.split('\n');
    const firstLine = lines.splice(0, 1)[0];

    let compareUrl;

    // first release
    if (firstLine.startsWith('# Changelog')) {
      // remove top comments
      lines.splice(0, 4);
    } else {
      compareUrl = firstLine.replace(/^.*\]\(([^)]+)\).*$/g, '$1');
    }

    changes = lines.join('\n');

    // remove whitespaces
    changes = changes.trim();

    // replace links
    changes = changes.replace(/\[([^\]]+)\]\(([^)]+)\)/g, '<$2|$1>');

    // shorten commit links
    changes = changes.replace(/\/commit\/([0-9a-f]{7})[0-9a-f]*\|/g, '/commit/$1|');

    // replace header formatting
    changes = changes.replace(/^### (.*)$/gm, '### *$1*');

    // reword breaking changes header
    changes = changes.replace(`### *⚠ ${breakingChangesTitle}*`, `### ${breakingChangesTitle}`);

    // reword other headers
    const headers: Array<{ title: string; emoji: string }> = [
      { title: 'Features', emoji: 'sparkles' },
      { title: 'Bug Fixes', emoji: 'bug' },
      { title: 'Documentation', emoji: 'books' },
      { title: 'Styling', emoji: 'gem' },
      { title: 'Refactoring', emoji: 'hammer' },
      { title: 'Performance Improvements', emoji: 'rocket' },
      { title: 'Testing', emoji: 'rotating_light' },
      { title: 'Build', emoji: 'package' },
      { title: 'CI', emoji: 'construction_worker' },
      { title: 'Chores', emoji: 'broom' },
    ];

    headers.forEach(header => {
      changes = changes.replace(`### *${header.title}*`, `### :${header.emoji}: *${header.title}*`);
    });

    // replace bold formatings
    changes = changes.replace(/\*\*/g, '*');

    // split to sections
    let sections: ContentSection[] = changes
      .split('### ')
      .filter(s => s !== '')
      .map(section => {
        let sectionLines = section.split('\n');

        const sectionHeader = sectionLines.splice(0, 1)[0];
        sectionLines = sectionLines.filter(s => s !== '');

        // remove list indicators
        sectionLines = sectionLines.map(line => line.replace(/^\* (.*)$/gm, '$1'));

        return {
          header: sectionHeader,
          lines: sectionLines.map(line => ({ text: line, breaking: false })),
        };
      });

    // only on having breaking changes
    const breakingSection = sections.find(s => s.header === breakingChangesTitle);
    if (breakingSection) {
      sections = sections.filter(s => s.header !== breakingChangesTitle);

      breakingSection.lines.forEach(breakingLine => {
        sections.forEach(section => {
          section.lines.forEach(line => {
            const breaking = line.text.startsWith(breakingLine.text);

            if (breaking && !line.breaking) {
              line.breaking = true;
            }
          });
        });
      });
    }

    return {
      compareUrl,
      sections,
      breaking: !!breakingSection,
    };
  }
}
