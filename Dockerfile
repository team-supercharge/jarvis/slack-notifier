FROM node:18.20.0

# install envsubst
RUN apt-get update && apt-get install -y gettext-base

WORKDIR /app
COPY . .
RUN npm ci && npm run build

WORKDIR /
RUN npm install -g /app
