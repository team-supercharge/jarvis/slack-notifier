# JARVIS Slack Notifier

Node.js tool to provide Slack notifications through a Slack App to deliver JARVIS notifications

## Getting Started

When installed as an NPM package, the tool exposes the `jsn <command>` binary to use with the following available commands:

- `jsn build-start` - sends a Slack message containin release information about the currently built release (tag)
- `jsn build-result` - updates previous message based on the `CI_JOB_STATUS` variable
- `jsn send-message <message> [blocksFilePath]` - allows you to send a message to Slack using plain text or the fancy Slack Block Kit.
  - `message`: The plain text message that you want to send to Slack. When it's used together with `blocksFilePath`, then this will be the content of the corresponding Slack push notification. This parameter is required.
  - `blocksFilePath`: The file path to the JSON file containing the Slack Block Kit descriptor. This parameter is optional.
  - eg.: `jsn send-message "Hello from jsn package!" ./blocks.json`

different statuses can be notified manually as well:

- `jsn build-success` - updates previous message with a success state
- `jsn build-failed` - updates previous message with a failed state
- `jsn build-canceled` - updates previous message with a canceled state

general commands:

- `jsn version` - prints out version number

## Usage

The **recommended** use of _JARVIS Slack Notifier_ in a platform-agnostic way is documented in the main JARVIS repository, [access the latest version here!](https://gitlab.com/team-supercharge/jarvis/jarvis#notification-templates)

**In Node-based projects**

If you are running a Node-based project you can add `@team-supercharge/jarvis-slack-notifier` to your `devDependencies` and use the commands with `npx` listed above, thus removing some extra build steps to be scheduled in your pipeline. For example if you have a single job producing your build:

```yaml
build:
  stage: build
  script:
    - npm ci # install package with "devDependencies"
    - npx jsn build-start
    - npm run build
  after_script:
    - npx jsn build-result
```

**Send message**

In addition to the `jsn send-message` command, the `jsn` package supports the use of Slack Block Kit to create custom message layouts. Slack Block Kit is a framework for building rich and interactive messages in Slack.

To create your own Slack Blocks, you can use the Slack Block Kit Builder which is a graphical user interface that allows you to create, preview and export Slack Block Kit layouts in JSON format. You can access the builder at https://app.slack.com/block-kit-builder.

To use custom Slack Blocks with the `jsn send-message` command, you can define them in a JSON file and provide the file path as the optional `[blocksFilePath]` parameter when running the command.

You can also parameterize your JSON input by using environment variables. You can set the variables, use them in your JSON template, then use `envsubst` to parse the variables and generate the final JSON message.

Here's an example of how you can use environment variables in your Slack Block Kit JSON template:

1. Define your Slack Block Kit layout using the Slack Block Kit Builder and export it as a JSON file. For example, let's say you have a layout that includes a text block and an image block:

```json
{
  "blocks": [
    {
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "${MESSAGE_TEXT}"
      }
    },
    {
      "type": "image",
      "image_url": "${IMAGE_URL}",
      "alt_text": "${IMAGE_ALT_TEXT}"
    }
  ]
}
```

2. Save the JSON file and define environment variables for the message text, image URL, and image alt text:

```bash
export MESSAGE_TEXT="Hello from jsn package"
export IMAGE_URL="https://cataas.com/cat/says/hello%20world"
export IMAGE_ALT_TEXT="Example Image"
export BLOCKS_FILE=blocks.json
```

3. Use the `envsubst` command to read the JSON file content and parse the environment variables and generate the final JSON message:

```bash
envsubst < $BLOCKS_FILE > __parsed_blocks.json
```

4. Use the `jsn send-message` command to send the message to Slack:

```bash
jsn send-message "Plain text message" __parsed_blocks.json
```

This will send a message to Slack with the text "Hello from jsn package" and an image with the URL "https://cataas.com/cat/says/hello%20world" and alt text "Example Image".

## Configuration

_JARVIS Slack Notifier_ gets its configuration entirely from environment variables:

| variable                            | required | description                                                        |
| ----------------------------------- | -------- | ------------------------------------------------------------------ |
| `JARVIS_SLACK_BOT_TOKEN`            | \*       | Slack App's bot token                                              |
| `JARVIS_SLACK_SIGNING_SECRET`       | \*       | Slack App's signing secret                                         |
| `JARVIS_SLACK_CHANNEL_ID`           | \*       | must be an ID, updating a message does not work with channel names |
| `JARVIS_SLACK_GROUP_EMOJI`          |          | emoji to use for project group (without colons)                    |
| `JARVIS_SLACK_GITLAB_PRIVATE_TOKEN` |          | use a private token instead of a job token (local testing)         |
| `JARVIS_SLACK_NO_RELEASE_CONTENT`   |          | if set to any value, no content (features, fixes, etc) sent        |

On top of that the project relies on a handful of GitLab environment variables to be defined as seen in the `.env.example` file.
