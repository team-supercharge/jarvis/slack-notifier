export enum ReleaseStatus {
  IN_PROGRESS = 'IN_PROGRESS',
  READY = 'READY',
  FAILED = 'FAILED',
  CANCELED = 'CANCELED',
}
