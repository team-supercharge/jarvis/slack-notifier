import { App, LogLevel } from '@slack/bolt';

import { ReleaseMessageModel } from '../interfaces/release-message-model';
import { ReleaseMessage } from '../messages/release-message';
import { ReleaseStatus } from '../enums/release-status';
import { BlockKitObject } from '../interfaces/block-kit-object';

export class SlackService {
  private readonly api: App;

  constructor() {
    this.api = new App({
      token: process.env.JARVIS_SLACK_BOT_TOKEN,
      signingSecret: process.env.JARVIS_SLACK_SIGNING_SECRET,
      logLevel: LogLevel.DEBUG,
    });
  }

  public async sendReleaseNotification(model: ReleaseMessageModel): Promise<string> {
    const message = new ReleaseMessage(model, ReleaseStatus.IN_PROGRESS);

    const blocks = message.blocks();
    console.log(JSON.stringify({ blocks }));

    const result = await this.api.client.chat.postMessage({
      channel: process.env.JARVIS_SLACK_CHANNEL_ID,
      text: model.name,
      blocks,
    });

    if (!result.ok) {
      throw Error('Could not send Slack notification');
    }

    return result.ts || 'error';
  }

  public async updateReleaseNotification(ts: string, model: ReleaseMessageModel, status: ReleaseStatus): Promise<string> {
    const message = new ReleaseMessage(model, status);

    const blocks = message.blocks();
    console.log(JSON.stringify({ blocks }));

    const result = await this.api.client.chat.update({
      channel: process.env.JARVIS_SLACK_CHANNEL_ID,
      ts,
      text: model.name,
      blocks,
    });

    if (!result.ok) {
      throw Error('Could not update message');
    }

    return result.ts || 'error';
  }

  public async sendMessage(message: string, blockKitObject: BlockKitObject | undefined): Promise<string> {
    console.log('Custom message:', message);

    console.log(JSON.stringify({ blocks: blockKitObject }));

    const result = await this.api.client.chat.postMessage({
      channel: process.env.JARVIS_SLACK_CHANNEL_ID,
      text: message,
      blocks: blockKitObject?.blocks,
    });

    if (!result.ok) {
      throw Error('Could not send Slack notification with custom message');
    }

    return result.ts || 'error';
  }
}
