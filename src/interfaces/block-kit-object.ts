import { Block } from '@slack/bolt';

export interface BlockKitObject {
  blocks: Block[];
}
