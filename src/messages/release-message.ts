import { Block, ContextBlock, SectionBlock } from '@slack/bolt';

import { ReleaseMessageModel } from '../interfaces/release-message-model';
import { ReleaseStatus } from '../enums/release-status';
import { ContentLine } from '../interfaces/content-line';

const MAX_SECTION_TEXT_LENGTH = 3000;

export class ReleaseMessage {
  constructor(private readonly model: ReleaseMessageModel, private readonly status: ReleaseStatus) {}

  public blocks(): Block[] {
    const blocks: Block[] = [];

    // divider
    blocks.push({
      type: 'divider',
    });

    // release header
    const groupEmoji = process.env.JARVIS_SLACK_GROUP_EMOJI || 'open_file_folder';
    const groupLink = `<${this.model.namespaceUrl}|:${groupEmoji}:>`;
    const projectLink = `<${this.model.project.url}|${this.model.project.title}>`;
    const breaking = this.model.breaking ? ':boom: ' : '';
    const releaseName = `${this.model.name}`;
    const header: SectionBlock = {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `${groupLink} ➔ *${projectLink}* ➔ ${breaking}*${releaseName}*`,
      },
    };

    blocks.push(header);

    const compareText = this.model.compareUrl ? `<${this.model.compareUrl}|Compare changes>` : `N/A`;

    // release metadata
    const metadata: ContextBlock = {
      type: 'context',
      elements: [
        {
          type: 'mrkdwn',
          text: `:label: <${this.model.tag.url}|${this.model.tag.name}>`,
        },
        {
          type: 'mrkdwn',
          text: `:pushpin: <${this.model.commit.url}|${this.model.commit.name}>`,
        },
        {
          type: 'mrkdwn',
          text: `:scales: ${compareText}`,
        },
      ],
    };

    if (this.model.sourceCodeUrl) {
      metadata.elements?.push({
        type: 'mrkdwn',
        text: `:package: <${this.model.sourceCodeUrl}|Source code (zip)>`,
      });
    }

    blocks.push(metadata);

    if (!process.env.JARVIS_SLACK_NO_RELEASE_CONTENT) {
      // content
      this.model.contentSections.forEach(section => {
        const sectionHeader: SectionBlock = {
          type: 'section',
          text: {
            type: 'mrkdwn',
            text: section.header,
          },
        };

        const sectionChunks = this.partition(section.lines).map(chunk => {
          const sectionChunk: SectionBlock = {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: chunk,
            },
          };

          return sectionChunk;
        });

        blocks.push(sectionHeader);
        blocks.push(...sectionChunks);
      });
    }

    // pipeline
    let statusIcon;
    let statusText;
    switch (this.status) {
      case ReleaseStatus.IN_PROGRESS:
        statusIcon = ':arrows_counterclockwise:';
        statusText = 'IN PROGRESS';
        break;

      case ReleaseStatus.READY:
        statusIcon = ':white_check_mark:';
        statusText = 'READY';
        break;

      case ReleaseStatus.FAILED:
        statusIcon = ':x:';
        statusText = 'FAILED';
        break;

      case ReleaseStatus.CANCELED:
        statusIcon = ':heavy_multiplication_x:';
        statusText = 'CANCELED';
        break;

      default:
        statusIcon = '';
        statusText = '';
        break;
    }

    const pipeline: ContextBlock = {
      type: 'context',
      elements: [
        {
          type: 'image',
          image_url: this.model.pipeline.user.avatarUrl,
          alt_text: this.model.pipeline.user.name,
        },
        {
          type: 'mrkdwn',
          text: this.model.pipeline.user.name,
        },
        {
          type: 'plain_text',
          text: '|',
        },
        {
          type: 'mrkdwn',
          text: `${statusIcon} <${this.model.pipeline.url}|${statusText}>`,
        },
      ],
    };

    blocks.push(pipeline);

    return blocks;
  }

  private partition(lines: ContentLine[]): string[] {
    const chunks: string[] = [];

    let chunk = '';

    lines.forEach(l => {
      // format line
      const line = `>- ${l.breaking ? ':boom: ' : ''}${l.text}\n`;

      // check if a new chunk is needed
      if (chunk.length + line.length > MAX_SECTION_TEXT_LENGTH) {
        chunks.push(chunk);
        chunk = '';
      }
      chunk += line;
    });

    chunks.push(chunk);

    return chunks;
  }
}
