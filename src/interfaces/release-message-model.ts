import { ContentSection } from './content-section';

export interface ReleaseMessageModel {
  name: string;
  breaking: boolean;
  compareUrl?: string;
  sourceCodeUrl?: string;
  namespaceUrl: string;
  project: {
    title: string;
    url: string;
  };
  tag: {
    name: string;
    url: string;
  };
  commit: {
    name: string;
    url: string;
  };
  contentSections: ContentSection[];
  pipeline: {
    user: {
      name: string;
      profileUrl: string;
      avatarUrl: string;
    };
    url: string;
  };
}
