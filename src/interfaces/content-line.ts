export interface ContentLine {
  breaking: boolean;
  text: string;
}
